///
/// \author Charles Hosson (charles.hosson@polymtl.ca)
///


#include <core/Application.hpp>


core::Application app;


void setup ( )
{
	app.setup();
}

void loop ( )
{
	app.loop();
}
