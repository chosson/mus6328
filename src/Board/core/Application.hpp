///
/// \author Charles Hosson (charles.hosson@polymtl.ca)
///

#pragma once


#pragma region "Includes" //{

#include <cstddef>
#include <cstdint>
#include <new>

#include <pin_io/Pwm.hpp>
#include <pin_io/DigitalOut.hpp>
#include <pin_io/DigitalIn.hpp>
#include <pin_io/TriggeredPulse.hpp>
#include <sensors/Adc.hpp>
#include <sensors/HcSr04.hpp>
#include <utils/RateTimer.hpp>
#include <rx_tx/Slip.hpp>
#include <rx_tx/SerialSensorsPacket.hpp>

#pragma endregion //}


namespace core
{

class Application
{
public:
	Application ( ) = default;
	Application ( const Application& ) = delete;

	~Application ( )
	{
		
	}

	void setup ( )
	{
		initSerial();

		m_switch1.init(D2);

		m_distanceIR1.init(A0);
		m_distanceIR2.init(A1);
		m_knob.init(A2);

		m_ledBuiltin.init(LED_BUILTIN);
		m_led1.init(D5);

		m_distanceSonar.init(D4, D6);

		delay(10);
	}

	void loop ( )
	{
		updateSensors();

		rx_tx::SensorDataPacket pkt = {};
		pkt.timestamp = millis();
		pkt.distIr1   = m_distanceIR1.getValue();
		pkt.distIr2   = m_distanceIR2.getValue();
		pkt.distSonar = m_distanceSonar.getDistanceCm();
		pkt.knob1     = m_knob.getValue();

		m_led1.write(pkt.distIr1);
		m_ledBuiltin.write(pkt.knob1);

		if ( m_loopSerial.next() and m_switch1.read() )
			m_slip.encodeAndWrite(&pkt, sizeof(pkt));
	}

private:
	void initSerial ( )
	{
		static rx_tx::Slip::ReadFn readWaiting = [] ( )
		{
			while ( true ) {
				int val = Serial.read();
				if ( val != -1 )
					return (uint8_t)val;
			}
		};
		static rx_tx::Slip::WriteFn write = [] ( uint8_t b ) { Serial.write(b); };

		Serial.begin(256'000);
		m_slip = rx_tx::Slip(m_serialBuffer, sizeof(m_serialBuffer), readWaiting, write);
	}

	void updateSensors ( )
	{
		// Turns out that reading from ADC is really slow on Arduino for some reason (analogRead takes ~50 us).
		// Therefore We update the more time-sensitive sonars in between each ADC read.
		m_distanceSonar.update();
		m_distanceIR1.update();
		m_distanceSonar.update();
		m_distanceIR2.update();
		m_distanceSonar.update();
		m_knob.update();
		m_distanceSonar.update();
	}

	pin_io::Pwm       m_ledBuiltin;
	pin_io::Pwm       m_led1;
	pin_io::DigitalIn m_switch1;
	sensors::Adc      m_distanceIR1;
	sensors::Adc      m_distanceIR2;
	sensors::Adc      m_knob;
	sensors::HcSr04   m_distanceSonar;
	uint8_t           m_serialBuffer[1024];
	rx_tx::Slip       m_slip;
	utils::RateTimer  m_loopSerial = 100.0f;
};

}

