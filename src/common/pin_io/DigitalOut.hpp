///
/// \author Charles Hosson (charles.hosson@polymtl.ca)
///

#pragma once


#pragma region "Includes" //{

#include <cstddef>
#include <cstdint>
#include <new>

#pragma endregion //}


namespace pin_io
{

class DigitalOut
{
public:
	DigitalOut ( ) = default;

	DigitalOut ( int pin )
	: m_pin(pin)
	{
		pinMode(m_pin, OUTPUT);
	}

	void init ( int pin )
	{
		*this = DigitalOut(pin);
	}

	void write ( bool value )
	{
		digitalWrite(m_pin, value);
	}

	void toggle ( )
	{
		digitalToggle(m_pin);
	}

	int getPin ( ) const { return m_pin; }

private:
	int m_pin = -1;
};

}

