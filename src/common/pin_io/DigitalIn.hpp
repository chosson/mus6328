///
/// \author Charles Hosson (charles.hosson@polymtl.ca)
///

#pragma once


#pragma region "Includes" //{

#include <cstddef>
#include <cstdint>
#include <new>

#include <functional>

#pragma endregion //}


namespace pin_io
{

class DigitalIn
{
public:
	using IsrCallback = std::function<void()>;

	DigitalIn ( ) = default;

	DigitalIn ( int pin, int mode = INPUT_PULLDOWN )
	: m_pin(pin)
	{
		pinMode(m_pin, mode);
	}

	void init ( int pin, int mode = INPUT_PULLDOWN )
	{
		*this = DigitalIn(pin, mode);
	}

	void setIsr ( IsrCallback callback, int mode = RISING )
	{
		attachInterrupt(m_pin, callback, mode);
	}

	void unsetIsr ( )
	{
		detachInterrupt(m_pin);
	}

	bool read ( )
	{
		return digitalRead(m_pin);
	}

	int readPulseUs ( bool state )
	{
		return pulseIn(m_pin, state);
	}

	int getPin ( ) const { return m_pin; }

private:
	int m_pin = -1;
};

}

