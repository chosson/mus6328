///
/// \author Charles Hosson (charles.hosson@polymtl.ca)
///

#pragma once


#pragma region "Includes" //{

#include <cstddef>
#include <cstdint>
#include <new>

#include <cmath>

#pragma endregion //}


namespace pin_io
{

class Pwm
{
public:
	static void setFrequencyHz ( int freqHz )
	{
		analogWriteFrequency(freqHz);
	}

	Pwm ( ) = default;

	Pwm ( int pin )
	: m_pin(pin)
	{
		pinMode(m_pin, OUTPUT);
	}

	void init ( int pin )
	{
		*this = Pwm(pin);
	}

	void write ( float value )
	{
		static constexpr int maxVal = 255;
		analogWrite(m_pin, (int)round(value * maxVal));
	}

	int getPin ( ) const { return m_pin; }

private:
	int m_pin = -1;
};

}

