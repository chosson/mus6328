///
/// \author Charles Hosson (charles.hosson@polymtl.ca)
///

#pragma once


#pragma region "Includes" //{

#include <cstddef>
#include <cstdint>
#include <new>

#pragma endregion //}


namespace pin_io
{

class AnalogIn
{
public:

	AnalogIn ( ) = default;

	AnalogIn ( int pin )
	: m_pin(pin)
	{
		static bool init = [](){analogReadResolution(resolutionBits); return true;}(); (void)init;
		pinMode(m_pin, INPUT_ANALOG);
	}

	void init ( int pin )
	{
		*this = AnalogIn(pin);
	}

	float read ( )
	{
		return analogRead(m_pin) / (float)maxValue;
	}

	int getPin ( ) const { return m_pin; }

private:
	static constexpr int resolutionBits = 12;
	static constexpr int maxValue = (1 << resolutionBits) - 1;

	int m_pin = -1;
};

}

