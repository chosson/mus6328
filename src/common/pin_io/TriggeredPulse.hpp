///
/// \author Charles Hosson (charles.hosson@polymtl.ca)
///

#pragma once


#pragma region "Includes" //{

#include <cstddef>
#include <cstdint>
#include <new>

#include <chrono>

#include <pin_io/DigitalOut.hpp>
#include <pin_io/DigitalIn.hpp>

#pragma endregion //}


namespace pin_io
{

class TriggeredPulse
{
public:

	TriggeredPulse ( ) = default;

	TriggeredPulse ( int trigPin, int echoPin, int triggerLengthUs, bool pulseValue )
	: m_trigger(trigPin),
	  m_echo(echoPin),
	  m_triggerLengthUs(triggerLengthUs),
	  m_pulseValue(pulseValue)
	{
		m_trigger.write(false);
		stopAsyncUpdates();
	}

	~TriggeredPulse ( )
	{
		stopAsyncUpdates();
	}

	void init ( int trigPin, int echoPin, int triggerLengthUs, bool pulseValue )
	{
		*this = TriggeredPulse(trigPin, echoPin, triggerLengthUs, pulseValue);
	}

	void startAsyncUpdates ( )
	{
		stopAsyncUpdates();
		m_lastUpdate = 0;
		m_echo.setIsr([this](){ updatePulseState(); }, CHANGE);
	}

	void stopAsyncUpdates ( )
	{
		m_waitingForPulseEnd = false;
		m_echo.unsetIsr();
	}

	void trigger ( )
	{
		m_trigger.write(true);
		delayMicroseconds(10);
		m_trigger.write(false);
		m_waitingForPulseBegin = true;
		m_waitingForPulseEnd = false;
	}

	int readUs ( )
	{
		return m_echoLength;
	}

	int triggerAndWait ( )
	{
		trigger();
		m_echoLength = m_echo.readPulseUs(m_pulseValue);

		return readUs();
	}

	void updatePulseState ( )
	{
		int64_t now = micros();
		bool pinValue = m_echo.read();
		if ( pinValue == not m_pulseValue and m_waitingForPulseEnd ) {
			if ( now >= m_lastUpdate )
				m_echoLength = now - m_lastUpdate;
			else
				m_echoLength = UINT32_MAX - (now - m_lastUpdate);
			m_waitingForPulseEnd = false;
			m_waitingForPulseBegin = false;
		} else if ( pinValue == m_pulseValue and m_waitingForPulseBegin ) {
			m_lastUpdate = now;
			m_waitingForPulseEnd = true;
			m_waitingForPulseBegin = false;
		}
	}

	int  getTriggerPin ( ) const { return m_trigger.getPin(); }
	int  getEchoPin ( ) const { return m_echo.getPin(); }
	int  getTriggerLength ( ) const { return m_triggerLengthUs; }
	bool getPulseValue ( ) const { return m_pulseValue; }
	bool isWaitingForPulseBegin ( ) const { return m_waitingForPulseBegin; }
	bool isWaitingForPulseEnd ( ) const { return m_waitingForPulseEnd; }

private:
	pin_io::DigitalOut m_trigger;
	pin_io::DigitalIn  m_echo;
	int                m_triggerLengthUs;
	bool               m_pulseValue;
	volatile int       m_echoLength = 0;
	volatile int64_t   m_lastUpdate = 0;
	volatile bool      m_waitingForPulseBegin = false;
	volatile bool      m_waitingForPulseEnd = false;
};

}

