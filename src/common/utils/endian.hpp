////////////////////////////////////////////////////////////////////////////////
/// \file endian.hpp
////////////////////////////////////////////////////////////////////////////////

#pragma once


#pragma region "Includes" //{

#include <ciso646>
#include <cstddef>
#include <cstdint>
#include <new>

#include <boost/endian/buffers.hpp>

#pragma endregion //}


namespace utils
{

#pragma region "Declarations" //{

#pragma region "Forward Declarations" //{
/// \cond

template < size_t >
class IntBE;
template < size_t >
class UIntBE;

/// \endcond
#pragma endregion //}


#pragma region "Classes" //{

class FloatBE
{
public:
	FloatBE() = default;
	FloatBE(float newVal) { *this = newVal; }
#ifdef NATIVE_BIGENDIAN
public:
	operator float() const { return val; }
	float operator=(float newVal) { return val = newVal; }
private:
	float val;
#else
public:
	operator float() const { uint32_t nativeVal = val.value(); return *(float*)&nativeVal; }
	float operator=(float newVal) { uint32_t nativeVal; *(float*)&nativeVal = newVal; val = nativeVal; return newVal; }
private:
	boost::endian::big_uint32_buf_at val;
#endif
};


class DoubleBE
{
public:
	DoubleBE() = default;
	DoubleBE(double newVal) { *this = newVal; }
#ifdef NATIVE_BIGENDIAN
public:
	operator double() const { return val; }
	double operator=(double newVal) { return val = newVal; }
private:
	double val;
#else
public:
	operator double() const { uint64_t nativeVal = val.value(); return *(double*)&nativeVal; }
	double operator=(double newVal) { uint64_t nativeVal; *(double*)&nativeVal = newVal; val = nativeVal; return newVal; }
private:
	boost::endian::big_uint64_buf_at val;
#endif
};


template < >
class IntBE<16>
{
public:
	IntBE() = default;
	IntBE(int16_t newVal) { *this = newVal; }
#ifdef NATIVE_BIGENDIAN
public:
	operator int16_t() const { return val; }
	int16_t operator=(int16_t newVal) { return val = newVal; }
private:
	int16_t val;
#else
public:
	operator int16_t() const { uint16_t nativeVal = val.value(); return *(int16_t*)&nativeVal; }
	int16_t operator=(int16_t newVal) { uint16_t nativeVal; *(int16_t*)&nativeVal = newVal; val = nativeVal; return newVal; }
private:
	boost::endian::big_uint16_buf_at val;
#endif
};


template < >
class IntBE<32>
{
public:
	IntBE() = default;
	IntBE(int32_t newVal) { *this = newVal; }
#ifdef NATIVE_BIGENDIAN
public:
	operator int32_t() const { return val; }
	int32_t operator=(int32_t newVal) { return val = newVal; }
private:
	int32_t val;
#else
public:
	operator int32_t() const { uint32_t nativeVal = val.value(); return *(int32_t*)&nativeVal; }
	int32_t operator=(int32_t newVal) { uint32_t nativeVal; *(int32_t*)&nativeVal = newVal; val = nativeVal; return newVal; }
private:
	boost::endian::big_uint32_buf_at val;
#endif
};


template < >
class IntBE<64>
{
public:
	IntBE() = default;
	IntBE(int64_t newVal) { *this = newVal; }
#ifdef NATIVE_BIGENDIAN
public:
	operator int64_t() const { return val; }
	int64_t operator=(int64_t newVal) { return val = newVal; }
private:
	int64_t val;
#else
public:
	operator int64_t() const { uint64_t nativeVal = val.value(); return *(int64_t*)&nativeVal; }
	int64_t operator=(int64_t newVal) { uint64_t nativeVal; *(int64_t*)&nativeVal = newVal; val = nativeVal; return newVal; }
private:
	boost::endian::big_uint64_buf_at val;
#endif
};


template < >
class UIntBE<16>
{
public:
	UIntBE() = default;
	UIntBE(uint16_t newVal) { *this = newVal; }
#ifdef NATIVE_BIGENDIAN
public:
	operator uint16_t() const { return val; }
	uint16_t operator=(uint16_t newVal) { return val = newVal; }
private:
	uint16_t val;
#else
public:
	operator uint16_t() const { uint16_t nativeVal = val.value(); return *(uint16_t*)&nativeVal; }
	uint16_t operator=(uint16_t newVal) { uint16_t nativeVal; *(uint16_t*)&nativeVal = newVal; val = nativeVal; return newVal; }
private:
	boost::endian::big_uint16_buf_at val;
#endif
};


template < >
class UIntBE<32>
{
public:
	UIntBE() = default;
	UIntBE(uint32_t newVal) { *this = newVal; }
#ifdef NATIVE_BIGENDIAN
public:
	operator uint32_t() const { return val; }
	uint32_t operator=(uint32_t newVal) { return val = newVal; }
private:
	uint32_t val;
#else
public:
	operator uint32_t() const { uint32_t nativeVal = val.value(); return *(uint32_t*)&nativeVal; }
	uint32_t operator=(uint32_t newVal) { uint32_t nativeVal; *(uint32_t*)&nativeVal = newVal; val = nativeVal; return newVal; }
private:
	boost::endian::big_uint32_buf_at val;
#endif
};


template < >
class UIntBE<64>
{
public:
	UIntBE() = default;
	UIntBE(uint64_t newVal) { *this = newVal; }
#ifdef NATIVE_BIGENDIAN
public:
	operator uint64_t() const { return val; }
	uint64_t operator=(uint64_t newVal) { return val = newVal; }
private:
	uint64_t val;
#else
public:
	operator uint64_t() const { uint64_t nativeVal = val.value(); return *(uint64_t*)&nativeVal; }
	uint64_t operator=(uint64_t newVal) { uint64_t nativeVal; *(uint64_t*)&nativeVal = newVal; val = nativeVal; return newVal; }
private:
	boost::endian::big_uint64_buf_at val;
#endif
};

#pragma endregion //}

#pragma endregion //}

}

