///
/// \author Charles Hosson (charles.hosson@polymtl.ca)
///

#pragma once


#pragma region "Includes" //{

#include <cstddef>
#include <cstdint>
#include <new>

#include <ctime>

#pragma endregion //}


namespace utils
{

// Example usage (60 Hz):
//   RateTimer timedLoop = 60.0f
//   while ( main program loop condition ) {
//     The stuff that happens ASAP...
//     if ( timedLoop.next() ) {
//       The stuff that happens at 60 hz...
//     }
//   }
class RateTimer
{
public:
	RateTimer ( float frequencyHz )
	: m_nextTimeMs(getCurrentTimeMs()),
	  m_periodMs(int(1000 / frequencyHz))
	{ }

	bool next ( )
	{
		int currentMs = getCurrentTimeMs();
		if ( currentMs < m_nextTimeMs - m_periodMs )
			m_nextTimeMs = currentMs;

		if ( currentMs < m_nextTimeMs )
			return false;
		
		// We are late by more than one period; this might happen if we are too slow or if the device has been on sleep while we were waiting.
		if ( currentMs > m_nextTimeMs )
			m_nextTimeMs = currentMs;

		m_nextTimeMs += m_periodMs;
		return true;
	}

private:
	static int getCurrentTimeMs ( )
	{
		#ifdef ARDUINO
			return (int)millis();
		#else
			return int((float)clock() / CLOCKS_PER_SEC * 1000);
		#endif
	}

	int m_nextTimeMs;
	int m_periodMs;
};

}

