///
/// \author Charles Hosson (charles.hosson@polymtl.ca)
///

#pragma once


#pragma region "Includes" //{

#include <ciso646>
#include <cstddef>
#include <cstdint>
#include <new>

#pragma endregion //}


#pragma region "Declarations" //{

#pragma region "Macros" //{

// Packs macro argument that contains commas
#define MACRO_ARG(...) __VA_ARGS__

// Puts in quotes
#define STRINGIZE(...) #__VA_ARGS__

// Creates a packed struct (no alignment padding)
#if defined(__GNUC__) || defined(__clang__)
    #define PACKED_DECL(...) __VA_ARGS__ __attribute__((__packed__))
#elif defined(_MSC_VER)
    #define PACKED_DECL(...) __pragma(pack(push, 1)) __VA_ARGS__; __pragma(pack(pop))
#else
    #define PACKED_DECL(...) static_assert(false, "Compiler not supported for PACKED_DECL() macro.")
#endif

// Declare a char array padded for a given number of bytes
#define DECL_PADDED_CHAR_ARRAY_STR(name, str, alignment) \
char name[(sizeof(str) % alignment == 0) ? sizeof(str) : sizeof(str) + alignment - sizeof(str) % alignment] = str; \

#pragma endregion //}

#pragma endregion //}

