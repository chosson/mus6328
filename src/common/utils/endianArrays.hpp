////////////////////////////////////////////////////////////////////////////////
/// \file endianArrays.hpp
////////////////////////////////////////////////////////////////////////////////

#pragma once


#pragma region "Includes" //{

#include <ciso646>
#include <cstddef>
#include <cstdint>
#include <new>

#include <utils/endian.hpp>
#include <math/VCNVector.hpp>
#include <xsens/cmttypes.hpp>

#pragma endregion //}


namespace utils
{

#pragma region "Declarations" //{

#pragma region "Globals" //{

inline
void
cmtVecToArrayBE
( const CmtVector& vec, FloatBE* result )
{
	for (int i = 0; i < 3; i++)
		result[i] = FloatBE((float)vec[i]);
}

inline
void
cmtEulerToArrayBE
( const CmtEuler& eul, FloatBE* result )
{
	result[0] = FloatBE((float)eul.m_roll);
	result[1] = FloatBE((float)eul.m_pitch);
	result[2] = FloatBE((float)eul.m_yaw);
}

inline
void
vcnToArrayBE
( const VCNVector& vec, FloatBE* result )
{
	for ( int i = 0; i < 3; i++ )
		result[i] = FloatBE((float)vec[i]);
}

#pragma endregion //}

#pragma endregion //}

}

