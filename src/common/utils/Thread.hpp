///
/// \author Charles Hosson (charles.hosson@polymtl.ca)
///

#pragma once


#pragma region "Includes" //{

#include <cstddef>
#include <cstdint>
#include <new>

#include <algorithm>
#include <array>
#include <utility>
#include <functional>

#include <pin_io/DigitalIn.hpp>
#include <pin_io/Pwm.hpp>

#pragma endregion //}


namespace utils
{

// Code that is put in a "Thread" is run inside an interrupt, therefore it must be kept short and cannot use delay() or millis() properly.
class Thread
{
public:
	using ThreadFn = std::function<void()>;

	static constexpr int maxNumThreads = 4;

	static void setIntervalMs ( int intervalMs ) { callIntervalMs = intervalMs; }

	Thread ( ) = default;
	Thread ( const Thread& ) = delete;

	~Thread ( )
	{
		join();
	}

	void run ( ThreadFn&& fn )
	{
		static bool init = initThreads(); (void)init;

		if ( m_handle == nullptr )
			m_handle = takeNextHandle();
		m_function = std::move(fn);
	}

	void join ( )
	{
		if ( m_handle != nullptr )
			*m_handle = nullptr;
		m_handle = nullptr;
	}

private:
	using ThreadArray = std::array<Thread*, maxNumThreads>;

	static constexpr int pwmPin = D3;
	static constexpr int interruptPin = D2;

	inline static ThreadArray threads = {};
	inline static int         callIntervalMs = 1;

	static bool initThreads ( )
	{
		// Ok so this is a big ol' hack. Basically, output PWM on a pin that triggers interrupts on another input pin.
		// Only workaround that truly works for all platforms, at the cost of two now unusable digital pins.
		static pin_io::Pwm       signal(pwmPin);
		static pin_io::DigitalIn interrupt(interruptPin);
		signal.write(0.5);
		interrupt.setIsr(callThreads, RISING);
		return true;
	}

	static void callThreads ( )
	{
		static int lastRun = millis();
		int now = millis();
		if ( now - lastRun < callIntervalMs )
			return;

		lastRun = now;
		for ( auto&& thr : threads ) {
			if ( thr != nullptr )
				thr->call();
		}
	}

	Thread** takeNextHandle ( )
	{
		auto pos = std::find(threads.begin(), threads.end(), nullptr);
		if ( pos != threads.end() ) {
			*pos = this;
			return &*pos;
		} else {
			return nullptr;
		}
	}

	void call ( )
	{
		if ( m_function != nullptr )
			m_function();
	}

	ThreadFn m_function;
	Thread** m_handle = nullptr;
};

}

