///
/// \author Charles Hosson (charles.hosson@polymtl.ca)
///

#pragma once


#pragma region "Includes" //{

#include <cstddef>
#include <cstdint>
#include <new>

#include <utils/endian.hpp>
#include <utils/macros.hpp>

#pragma endregion //}


namespace rx_tx
{

struct OscSensorsPacket
{
	DECL_PADDED_CHAR_ARRAY_STR(address, "/mus6328/sensors", 4);
	DECL_PADDED_CHAR_ARRAY_STR(typeTags, ",i""ffff", 4);
	utils::IntBE<32> timestamp;
	utils::FloatBE   distIr1;
	utils::FloatBE   distIr2;
	utils::FloatBE   distSonar;
	utils::FloatBE   knob1;
};

}

