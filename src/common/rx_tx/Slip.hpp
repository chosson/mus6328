///
/// \author Charles Hosson (charles.hosson@polymtl.ca)
///

#pragma once


#pragma region "Includes" //{

#include <cstddef>
#include <cstdint>
#include <new>

#include <functional>
#include <utility>

#include <gsl/span>
#include <gsl/span_ext>
#include <cppitertools/range.hpp>

#pragma endregion //}


namespace rx_tx
{

// SLIP : RFC 1055, https://tools.ietf.org/html/rfc1055
// Implementation inspired by https://github.com/marierm/mus3328-exemples/blob/master/protocoles/serial/slip
class Slip
{
public:
	using ReadFn = uint8_t(*)();
	using WriteFn = void(*)(uint8_t);

	Slip ( ) = default;

	Slip ( void* buffer, size_t size, ReadFn readFn = nullptr, WriteFn writeFn = nullptr )
	: m_buffer((uint8_t*)buffer),
	  m_bufferSize(size),
	  m_readFn(readFn),
	  m_writeFn(writeFn)
	{ }

	// After a call to encode(), getData() will return encoded packet.
	bool encode ( const void* data, size_t size )
	{
		bool ok = true;
		m_dataSize = 0;
		for ( auto b : gsl::make_span((const uint8_t*)data, size) ) {
			ok &= encodeByte(b);
			if ( not ok )
				return false;
		}
		ok &= append(Codes::end);
		return ok;
	}

	void encodeAndWrite ( const void* data, size_t size )
	{
		encode(data, size);
		for ( auto b : gsl::make_span(m_buffer, m_dataSize) )
			m_writeFn(b);
	}

	// After a call to readAndDecode(), getData() will return decoded packet (the packet payload).
	bool readAndDecode ( )
	{
		m_dataSize = 0;
		bool escaping = false;
		while ( true ) {
			uint8_t encodedByte = m_readFn();
			bool done;
			bool ok = decodeByte(encodedByte, escaping, done);
			if ( not ok )
				return false;
			if ( done )
				return true;
		}
	}

	template < typename T = void >
	std::pair<T*, size_t> getData ( ) const { return {reinterpret_cast<T*>(m_buffer), m_dataSize}; }

	bool isValid ( ) const
	{
		return m_buffer != nullptr and m_bufferSize != 0 and (m_readFn != nullptr or m_writeFn != nullptr);
	}

private:
	enum Codes : uint8_t
	{
		end =    0xC0,
		escape = 0xDB,
		escEnd = 0xDC,
		escEsc = 0xDD
	};

	bool encodeByte ( uint8_t dataByte )
	{
		bool ok = true;
		switch ( dataByte ) {
			case Codes::end :
				ok &= append(Codes::escape);
				ok &= append(Codes::escEnd);
				break;
			case Codes::escape :
				ok &= append(Codes::escape);
				ok &= append(Codes::escEsc);
				break;
			default :
				ok &= append(dataByte);
		}
		return ok;
	}

	bool decodeByte ( uint8_t encodedByte, bool& escaping, bool& end )
	{
		bool ok = true;
		end = false;
		if ( escaping ) {
			switch ( encodedByte ) {
				case Codes::escEnd : ok &= append(Codes::end); break;
				case Codes::escEsc : ok &= append(Codes::escape); break;
				default : { }
			}
			escaping = false;
		} else {
			switch ( encodedByte ) {
				case Codes::end : end = true; break;
				case Codes::escape : escaping = true; break;
				default : ok &= append(encodedByte);
			}
		}
		return ok;
	}

	bool append ( uint8_t rawByte )
	{
		if ( m_dataSize < m_bufferSize ) {
			*(m_buffer + m_dataSize++) = rawByte;
			return true;
		} else {
			return false;
		}
	}

	uint8_t* m_buffer = nullptr;
	size_t   m_bufferSize = 0;
	size_t   m_dataSize = 0;
	ReadFn   m_readFn = nullptr;
	WriteFn  m_writeFn = nullptr;
};

}

