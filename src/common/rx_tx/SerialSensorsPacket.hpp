///
/// \author Charles Hosson (charles.hosson@polymtl.ca)
///

#pragma once


#pragma region "Includes" //{

#include <cstddef>
#include <cstdint>
#include <new>

#pragma endregion //}


namespace rx_tx
{

struct SensorDataPacket
{
	uint32_t timestamp;
	float    distIr1;
	float    distIr2;
	float    distSonar;
	float    knob1;
};

}

