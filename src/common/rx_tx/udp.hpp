///
/// \author Charles Hosson (charles.hosson@polymtl.ca)
///

#pragma once


#pragma region "Includes" //{

#include <ciso646>
#include <cstddef>
#include <cstdint>
#include <new>

#include <stdexcept>
#include <string>

#include <boost/format.hpp>

#include <oscpkt/oscpkt.hh>
#include <oscpkt/udp.hh>

#pragma endregion //}


namespace rx_tx
{

#pragma region "Declarations" //{

#pragma region "Classes" //{

struct BindError : public std::runtime_error {
	BindError(int port_number, const std::string& message)
	: std::runtime_error(boost::str(boost::format("Error binding port %1% : %2%") % port_number % message))
	{ }
};
struct ConnectError : public std::runtime_error {
	ConnectError(const std::string& remote_host_name, int port_number, const std::string& message)
	: std::runtime_error(boost::str(boost::format("Error connecting to %1%:%2% : %3%") % remote_host_name % port_number % message))
	{ }
};

#pragma endregion //}


#pragma region "Globals" //{

template <typename SocketT>
SocketT bindUdpPort(int port_number)
{
	SocketT socket;
	socket.bindTo(port_number);
	if (!socket.isOk())
		throw BindError(port_number, socket.errorMessage());
	return std::move(socket);
}

template <typename SocketT>
SocketT connectToUdpPort(const std::string& remote_host_name, int port_number)
{
	SocketT socket;
	socket.connectTo(remote_host_name, port_number);
	if (!socket.isOk())
		throw ConnectError(remote_host_name, port_number, socket.errorMessage());
	return std::move(socket);
}

#pragma endregion //}

#pragma endregion //}

}

