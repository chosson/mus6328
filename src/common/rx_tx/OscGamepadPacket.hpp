///
/// \author Charles Hosson (charles.hosson@polymtl.ca)
///

#pragma once


#pragma region "Includes" //{

#include <cstddef>
#include <cstdint>
#include <new>

#include <cstring>

#include <utils/endian.hpp>
#include <utils/macros.hpp>

#pragma endregion //}


namespace rx_tx
{

struct OscGamepadPacket
{
	DECL_PADDED_CHAR_ARRAY_STR(address, "/mus6328/gamepad0", 4); // Change 0 for the gamepad number if different.
	DECL_PADDED_CHAR_ARRAY_STR(typeTags, ",i""iiii""iii""fff", 4);
	utils::IntBE<32> id;
	utils::IntBE<32> btnA;
	utils::IntBE<32> btnB;
	utils::IntBE<32> btnX;
	utils::IntBE<32> btnY;
	utils::IntBE<32> btnRB;
	utils::IntBE<32> btnStart;
	utils::IntBE<32> btnThumbR;
	utils::FloatBE   trigR;
	utils::FloatBE   stickRX;
	utils::FloatBE   stickRY;

};

struct OscGamepadRumblePacket
{
	DECL_PADDED_CHAR_ARRAY_STR(address, "/mus6328/gamepad0/rumble", 4); // Change 0 for the gamepad number if different.
	DECL_PADDED_CHAR_ARRAY_STR(typeTags, ",i""ff", 4);
	utils::IntBE<32> id;
	utils::FloatBE   left;
	utils::FloatBE   right;
};


char& getGamepadIdChar ( char* address )
{
	static int padNumberCharIndex = (int)strlen(rx_tx::OscGamepadPacket{}.address) - 1;
	return address[padNumberCharIndex];
}

char& getGamepadIdChar ( OscGamepadPacket& pkt )
{
	return getGamepadIdChar(pkt.address);
}

char& getGamepadIdChar ( OscGamepadRumblePacket& pkt )
{
	return getGamepadIdChar(pkt.address);
}

}

