///
/// \author Charles Hosson (charles.hosson@polymtl.ca)
///

#pragma once


#pragma region "Includes" //{

#include <cstddef>
#include <cstdint>
#include <new>

#include <pin_io/AnalogIn.hpp>

#pragma endregion //}


namespace sensors
{

class Adc
{
public:
	Adc ( ) = default;

	Adc ( int pin )
	: m_analogPin(pin)
	{ }

	void init ( int pin )
	{
		*this = Adc(pin);
	}

	void update ( )
	{
		m_value = m_analogPin.read();
	}

	float getValue ( ) const { return m_value; }
	int   getPin ( ) const { return m_analogPin.getPin(); }

//private:
	pin_io::AnalogIn m_analogPin;
	float            m_value;
};

}

