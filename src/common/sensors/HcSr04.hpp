///
/// \author Charles Hosson (charles.hosson@polymtl.ca)
///

#pragma once


#pragma region "Includes" //{

#include <cstddef>
#include <cstdint>
#include <new>

#include <pin_io/TriggeredPulse.hpp>

#pragma endregion //}


namespace sensors
{

class HcSr04
{
public:
	static constexpr int triggerLengthUs = 10;

	HcSr04 ( ) = default;

	HcSr04 ( int trigPin, int echoPin )
	: m_pulseSensor(trigPin, echoPin, triggerLengthUs, true)
	{ }

	void init ( int trigPin, int echoPin )
	{
		*this = HcSr04(trigPin, echoPin);
	}

	void initAsyncUpdate ( )
	{
		m_pulseSensor.startAsyncUpdates();
		m_isAsync = true;
	}

	void update ( bool waiting = false )
	{
		if ( m_isAsync ) {
			if ( not isUpdating() )
				m_pulseSensor.trigger();
		} else if ( waiting ) {
			m_pulseSensor.triggerAndWait();
		} else if ( not waiting ) {
			if ( isUpdating() )
				m_pulseSensor.updatePulseState();
			else
				m_pulseSensor.trigger();
		}

		updateDistanceValue();
	}

	float getDistanceCm ( ) const { return m_value; }
	float getDistanceInches ( ) const { return getDistanceCm() / inchesToCm; }
	int   getTriggerPin ( ) const { return m_pulseSensor.getEchoPin(); }
	int   getEchoPin ( ) const { return m_pulseSensor.getTriggerPin(); }
	bool  isUpdating ( ) const { return m_pulseSensor.isWaitingForPulseBegin() or m_pulseSensor.isWaitingForPulseEnd(); }

private:
	static constexpr float soundSpeed = 343; // m/s
	static constexpr float inchesToCm = 2.54; // cm/in
	static constexpr float usToCm = soundSpeed * 100.0 / 1'000'000.0; // cm/us
	static constexpr float adjustmentFactor = 1.0; // Experimental factor (applied to cm distance).
	static constexpr float adjustmentOffset = 0.0; // Experimental offset (applied to cm distance).

	void updateDistanceValue ( )
	{
		float measureCm = m_pulseSensor.readUs() * usToCm / 2; // Distance = half the travel time.
		m_value = measureCm * adjustmentFactor + adjustmentOffset;
	}

	pin_io::TriggeredPulse m_pulseSensor;
	float                  m_value;
	bool                   m_isAsync = false;
};

}

