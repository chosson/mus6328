///
/// \author Charles Hosson (charles.hosson@polymtl.ca)
///

#pragma once


#pragma region "Includes" //{

#include <cstddef>
#include <cstdint>
#include <new>

#include <string>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <stdexcept>
#include <thread>
#include <mutex>
#include <functional>

#include <gsl/span>
#include <cppitertools/range.hpp>
#include <serial/serial.h>

#include <rx_tx/Slip.hpp>
#include <rx_tx/udp.hpp>
#include <rx_tx/SerialSensorsPacket.hpp>
#include <rx_tx/OscSensorsPacket.hpp>
#include <rx_tx/OscGamepadPacket.hpp>
#include <utils/RateTimer.hpp>
#include <devices/Gamepad.hpp>

#pragma endregion //}


namespace core
{

class Application
{
public:
	Application ( ) = default;
	Application ( const Application& ) = delete;

	~Application ( )
	{
		
	}

	void setup ( int argc, char* argv[] )
	{
		if ( argc < 6 ) {
			std::cout << "Usage : <app> <serial port> <serial baud> <gamepad #> <osc input port> [<osc target>]" << std::endl;
			throw std::logic_error("Wrong usage");
		}

		std::string serialPort = argv[1];
		int serialBaudrate = std::stoi(argv[2]);
		try {
			initSerial(serialPort, serialBaudrate);
			std::cout << "Listening on serial port " << std::quoted(serialPort) << " at baud " << serialBaudrate << " bit/s." << std::endl;
		} catch ( serial::IOException& e ) {
			m_slip = rx_tx::Slip();
			std::cout << "Could not open serial port " << std::quoted(serialPort) << " : " << e.what() << std::endl;
		}

		int gamepadId = std::stoi(argv[3]);
		m_gamepads.emplace_back(gamepadId);
		std::cout << "Reading and controlling gamepad " << gamepadId << std::endl;

		m_oscInput.bindTo(std::stoi(argv[4]));
		std::cout << "Listening on OSC port " << m_oscInput.boundPort() << std::endl;

		std::cout << "Sending to " << std::endl;
		for ( int i : iter::range(5, argc) ) {
			addOscTarget(argv[i]);
			std::cout << "  " << m_oscTargets.back().remote_addr.asString() << std::endl;
		}
	}

	void exec ( )
	{
		if ( m_slip.isValid() )
			m_serialThread = std::thread(&Application::runSerialThread, this);
		m_gamepadThread = std::thread(&Application::runGamepadThread, this);

		while ( true ) { }
	}

private:
	static inline serial::Serial boardSerialPort;

	void initSerial ( const std::string& port, int baudrate )
	{
		static auto readWaiting = [] ( )
		{
			while ( true ) {
				uint8_t b;
				size_t sz = boardSerialPort.read(&b, 1);
				if ( sz == 1 )
					return b;
			}
		};
		static auto write = [] ( uint8_t b )
		{
			boardSerialPort.write(&b, 1);
		};

		auto to = serial::Timeout::simpleTimeout(serial::Timeout::max());
		boardSerialPort.setPort(port);
		boardSerialPort.setBaudrate(baudrate);
		boardSerialPort.setTimeout(to);
		boardSerialPort.open();
		boardSerialPort.flush();

		m_slip = rx_tx::Slip(m_serialBuffer, sizeof(m_serialBuffer), readWaiting, write);
	}

	void addOscTarget ( const std::string& target )
	{
		oscpkt::Url url(target);
		auto&& host = url.hostname;
		auto&& port = std::stoi(url.port);
		m_oscTargets.push_back(rx_tx::connectToUdpPort<oscpkt::UdpSocket>(host, port));
	}

	void buildAndSendSensorsPacket ( const rx_tx::SensorDataPacket& data )
	{
		using OscPacket = rx_tx::OscSensorsPacket;

		OscPacket pkt = {};
		pkt.timestamp = data.timestamp;
		pkt.distIr1   = data.distIr1;
		pkt.distIr2   = data.distIr2;
		pkt.distSonar = data.distSonar;
		pkt.knob1     = data.knob1;

		for ( auto&& socket : m_oscTargets ) {
			std::lock_guard lck(m_udpTxMutex);
			socket.sendPacket(&pkt, sizeof(pkt));
		}
	}

	void buildAndSendOscGamepadPackets ( )
	{
		for ( auto&& pad : m_gamepads ) {
			auto& pkt = m_gamepadPacketBuffer;
			rx_tx::getGamepadIdChar(pkt) = char('0' + pad.getId());
			pkt.id =        pad.getId();
			pkt.btnA =      pad.getBtnA();
			pkt.btnB =      pad.getBtnB();
			pkt.btnX =      pad.getBtnX();
			pkt.btnY =      pad.getBtnY();
			pkt.btnRB =     pad.getBtnRB();
			pkt.btnStart =  pad.getBtnStart();
			pkt.btnThumbR = pad.getBtnRThumb();
			pkt.trigR =     pad.getRTrigger();
			pkt.stickRX =   pad.getRStickX();
			pkt.stickRY =   pad.getRStickY();

			for ( auto&& socket : m_oscTargets ) {
				std::lock_guard lck(m_udpTxMutex);
				socket.sendPacket(&pkt, sizeof(pkt));
			}
		}
	}

	void runSerialThread ( )
	{
		while ( true ) {
			using rx_tx::SensorDataPacket;
			m_slip.readAndDecode();
			auto&& [sensors, sz] = m_slip.getData<SensorDataPacket>();
			if ( sz == sizeof(SensorDataPacket) )
				buildAndSendSensorsPacket(*sensors);
		}
	}

	void runGamepadThread ( )
	{
		utils::RateTimer loop = 120.0f;
		while ( true ) {
			if ( loop.next() ) {
				devices::Gamepad::updateAll();
				buildAndSendOscGamepadPackets();

 				bool receivedPacket = m_oscInput.receiveNextPacket(1);
				if ( receivedPacket and m_oscInput.packetSize() == sizeof(rx_tx::OscGamepadRumblePacket) ) {
					auto& data = *(rx_tx::OscGamepadRumblePacket*)m_oscInput.packetData();
					m_gamepads[data.id].vibrate(data.left, data.right);
				}
				// It seems that the windowing system stops the controller rumble on window focus change (no matter the window). We therefore have to repeat the vibration message on each frame to keep it vibrating.
				for ( auto&& pad : m_gamepads )
					pad.vibrate();
			}
		}
	}

	uint8_t                           m_serialBuffer[1024];
	rx_tx::Slip                       m_slip;
	std::vector<oscpkt::UdpSocket>    m_oscTargets;
	oscpkt::UdpSocket                 m_oscInput;
	std::vector<devices::GamepadXbox> m_gamepads;
	rx_tx::OscGamepadPacket           m_gamepadPacketBuffer = {};
	std::thread                       m_serialThread;
	std::thread                       m_gamepadThread;
	std::mutex                        m_udpTxMutex;
};

}

