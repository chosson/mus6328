///
/// \author Charles Hosson (charles.hosson@polymtl.ca)
///

#pragma once


#pragma region "Includes" //{

#include <cstddef>
#include <cstdint>
#include <new>

#include <vector>

#include <gainput/gainput.h>

#pragma endregion //}


namespace devices
{

class Gamepad
{
public:
	static void updateAll ( ) { manager.Update(); }

	Gamepad ( ) = default;

	Gamepad ( int id )
	{
		while ( (int)devices.size() < id + 1 )
			devices.push_back(manager.CreateAndGetDevice<PadDevice>());
		m_device = devices[id];
	}

	int getId ( ) const { return (int)m_device->GetDeviceId(); }

	void vibrateLeft ( float value )
	{
		vibrate(value, m_rightMotor);
	}
	
	void vibrateRight ( float value )
	{
		vibrate(m_leftMotor, value);
	}

	void vibrate ( float left, float right )
	{
		m_leftMotor = left;
		m_rightMotor = right;
		vibrate();
	}

	void vibrate ( )
	{
		m_device->Vibrate(m_leftMotor, m_rightMotor);
	}

protected:
	using PadDevice = gainput::InputDevicePad;

	static inline gainput::InputManager manager;
	static inline std::vector<PadDevice*> devices;

	bool getBtnState ( gainput::DeviceButtonId btn ) const
	{
		return m_device->GetBool(btn);
	}

	bool getBtnClicked ( gainput::DeviceButtonId btn ) const
	{
		return not m_device->GetBool(btn) and m_device->GetBoolPrevious(btn);
	}

	float getAxisValue ( gainput::DeviceButtonId axis ) const
	{
		return m_device->GetFloat(axis);
	}

	bool getAxisChanged ( gainput::DeviceButtonId axis ) const
	{
		return m_device->GetFloat(axis) != m_device->GetFloatPrevious(axis);
	}

	PadDevice* m_device = nullptr;
	float      m_leftMotor = 0.0f;
	float      m_rightMotor = 0.0f;
};

class GamepadXbox : public Gamepad
{
public:
	using Gamepad::Gamepad;

	bool  getBtnA ( ) const      { return getBtnState(gainput::PadButtonA); }
	bool  getBtnB ( ) const      { return getBtnState(gainput::PadButtonB); }
	bool  getBtnX ( ) const      { return getBtnState(gainput::PadButtonX); }
	bool  getBtnY ( ) const      { return getBtnState(gainput::PadButtonY); }
	bool  getBtnStart ( ) const  { return getBtnState(gainput::PadButtonStart); }
	bool  getBtnSelect ( ) const { return getBtnState(gainput::PadButtonSelect); }
	bool  getBtnBack ( ) const   { return getBtnState(gainput::PadButtonSelect); }
	bool  getBtnLB ( ) const     { return getBtnState(gainput::PadButtonL1); }
	bool  getBtnRB ( ) const     { return getBtnState(gainput::PadButtonR1); }
	bool  getBtnLThumb ( ) const { return getBtnState(gainput::PadButtonL3); }
	bool  getBtnRThumb ( ) const { return getBtnState(gainput::PadButtonR3); }
	float getLStickX ( ) const   { return getAxisValue(gainput::PadButtonLeftStickX); }
	float getLStickY ( ) const   { return getAxisValue(gainput::PadButtonLeftStickY); }
	float getRStickX ( ) const   { return getAxisValue(gainput::PadButtonRightStickX); }
	float getRStickY ( ) const   { return getAxisValue(gainput::PadButtonRightStickY); }
	float getLTrigger ( ) const  { return getAxisValue(gainput::PadButtonAxis4); }
	float getRTrigger ( ) const  { return getAxisValue(gainput::PadButtonAxis5); }

	bool getClickedBtnA ( ) const      { return getBtnClicked(gainput::PadButtonA); }
	bool getClickedBtnB ( ) const      { return getBtnClicked(gainput::PadButtonB); }
	bool getClickedBtnX ( ) const      { return getBtnClicked(gainput::PadButtonX); }
	bool getClickedBtnY ( ) const      { return getBtnClicked(gainput::PadButtonY); }
	bool getClickedBtnStart ( ) const  { return getBtnClicked(gainput::PadButtonStart); }
	bool getClickedBtnSelect ( ) const { return getBtnClicked(gainput::PadButtonSelect); }
	bool getClickedBtnBack ( ) const   { return getBtnClicked(gainput::PadButtonSelect); }
	bool getClickedBtnLB ( ) const     { return getBtnClicked(gainput::PadButtonL1); }
	bool getClickedBtnRB ( ) const     { return getBtnClicked(gainput::PadButtonR1); }
	bool getClickedBtnLThumb ( ) const { return getBtnClicked(gainput::PadButtonL3); }
	bool getClickedBtnRThumb ( ) const { return getBtnClicked(gainput::PadButtonR3); }
	bool getChangedLStickX ( ) const   { return getAxisChanged(gainput::PadButtonLeftStickX); }
	bool getChangedLStickY ( ) const   { return getAxisChanged(gainput::PadButtonLeftStickY); }
	bool getChangedRStickX ( ) const   { return getAxisChanged(gainput::PadButtonRightStickX); }
	bool getChangedRStickY ( ) const   { return getAxisChanged(gainput::PadButtonRightStickY); }
	bool getChangedLTrigger ( ) const  { return getAxisChanged(gainput::PadButtonAxis4); }
	bool getChangedRTrigger ( ) const  { return getAxisChanged(gainput::PadButtonAxis5); }
};

}

