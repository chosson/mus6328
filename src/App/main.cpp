///
/// \author Charles Hosson (charles.hosson@polymtl.ca)
///


#pragma region "Includes" //{

#include <cstddef>
#include <cstdint>
#include <new>

#include <core/Application.hpp>

using namespace std::literals;

#pragma endregion //}


int main ( int argc, char* argv[] )
{
	auto app = std::make_unique<core::Application>();
	app->setup(argc, argv);
	app->exec();
}

