/// \author Francois-R.Boyer@PolyMtl.ca
/// \date   2017-04
#pragma once
#include <type_traits>
#define CONST_TO_NONCONST_METHOD(METHOD_CALL) const_cast<std::remove_const_t<decltype(METHOD_CALL)>>(static_cast<const std::remove_reference_t<decltype(*this)>*>(this)->METHOD_CALL)
