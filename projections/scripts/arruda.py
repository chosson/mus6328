import time
import argparse
import collections
import asyncio

from pythonosc import osc_server
from pythonosc import dispatcher
import pyo


Sound = collections.namedtuple("Sound", ["player", "position"])


def parse_args():
	arg_parser = argparse.ArgumentParser()
	arg_parser.add_argument("--ip",
	                        help="IP to listen to")
	arg_parser.add_argument("--port", type=int,
	                        help="UDP port to listen to")
	arg_parser.add_argument("--device-id", type=int, dest="device_id",
	                        help="Portaudio output device ID")
	arg_parser.add_argument("--osc-addr", dest="osc_addr",
	                        help="OSC Address")
	arg_parser.add_argument("--sample-files", nargs="*", dest="sample_files",
	                        help="Audio sample files to use")

	return arg_parser.parse_args()

def list_and_prompt_device_id():
	pyo.pa_list_host_apis()
	pyo.pa_list_devices()
	return int(input("Enter output device ID : "))

def init_pyo_server(args):
	pyo_server = pyo.Server()
	if args.device_id != 0:
		pyo_server.setOutputDevice(args.device_id)
	pyo_server.boot()

	return pyo_server

def init_sounds(mode, args):
	sounds = []
	mixer = pyo.Mixer(outs=1)

	sample_files = []
	if mode == 0:
		sample_files = args.sample_files
	elif mode == 1:
		#notes = ["C4", "Cs4", "D4", "Ds4", "E4", "F4", "Fs4", "G4", "Gs4", "A4", "As4", "B4"]
		notes = ["C4", "Cs4", "Ds4", "F4", "Fs4", "Gs4", "B4"]
		for note in notes:
			sample_files.append("assets/" + note + ".wav")
	pos_offset = 1 / len(sample_files)
	for i, sf in enumerate(sample_files):
		player = pyo.SfPlayer(sf, loop=True, mul=1).play()
		sounds.append(Sound(player, i * pos_offset))
		mixer.addInput(i, player)
		mixer.setAmp(i, 0, 0)

	return sounds, mixer

def init_effects(mode, args, sounds, mixer):
	compressor = pyo.Compress(mixer[0], thresh=-1)
	reverb = pyo.Freeverb(compressor, size=1, damp=1)
	return [compressor, reverb]

def compute_distance_level(mode, pos1, pos2):
	diff = abs(pos2 - pos1)
	dist = min(diff, 1 - diff)
	return 0 if dist > 0.2 else (1 - dist - dist**2)**10

def apply_levels(mode, sounds, mixer, position, value):
	for i, snd in enumerate(sounds):
		lvl = compute_distance_level(mode, position, snd.position) * value**2
		mixer.setAmp(i, 0, lvl)

def apply_effects(mode, sounds, mixer, effects, reverb):
	effects[1].setSize(reverb)

def handle_msg(addr, *args):
	global position, value, reverb, mode
	position = args[0]
	value = args[1]
	reverb = args[2]
	mode = args[3]

position = 0
value = 0
reverb = 0
mode = 0

async def main():
	args = parse_args()

	if args.device_id is None:
		args.device_id = list_and_prompt_device_id()

	pyo_serv = init_pyo_server(args)

	ip = args.ip
	port = args.port
	osc_addr = args.osc_addr
	dispatch = dispatcher.Dispatcher()
	dispatch.map(osc_addr, handle_msg)
	osc_serv = osc_server.AsyncIOOSCUDPServer((ip, port), dispatch, asyncio.get_event_loop())
	transport = await osc_serv.create_serve_endpoint()

	pyo_serv.start()
	sounds = mixer = effects = None

	global position, value, reverb, mode
	current_mode = -1
	while True:
		if mode != current_mode:
			sounds, mixer = init_sounds(mode, args)
			effects = init_effects(mode, args, sounds, mixer)
			#mixer.out()
			for e in effects:
				e.out()
			current_mode = mode

		apply_levels(current_mode, sounds, mixer, position, value)
		apply_effects(current_mode, sounds, mixer, effects, reverb)

		await asyncio.sleep(0.01)

asyncio.run(main())

